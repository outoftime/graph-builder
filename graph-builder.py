#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import click
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import os


@click.command(help='http://matplotlib.org/users/mathtext.html')
@click.argument('file', type=click.File())
@click.option('-fx/-no-fx', default=False, help='Зафиксировать ось Х')
@click.option('-fy/-no-fy', default=False, help='Зафиксировать ось У')
@click.option('-r/-no-r', default=False, help='Поменять местами оси')
def build_graph(file, fx, fy, r):
    axes_names = file.readline().strip().split(';')
    values = np.array(np.mat(file.read().replace('\n', ';')))
    if r:
        axes_names = axes_names[::-1]
        values = np.fliplr(values)
    n = len(values)
    mx, my = np.mean(values[:, 0]), np.mean(values[:, 1])
    k = (sum([x*y for x, y in values]) / n - mx * my) / (sum([x**2 for x in values[:, 0]]) / n - mx ** 2)
    b = my - k*mx

    def linear(x):
        return x*k + b

    mpl.rcParams['savefig.directory'] = os.path.join(os.path.dirname(__file__), 'img')
    mpl.rc('font', **{'sans-serif': 'Arial', 'family': 'sans-serif'})
    fig, ax = plt.subplots()
    ax.plot(values[:, 0], linear(values[:, 0]))
    ax.plot(values[:, 0], values[:, 1], 'ro')
    ax.grid(True, which='both')
    # turn off the right spine/ticks
    ax.spines['right'].set_color('none')
    # turn off the top spine/ticks
    ax.spines['top'].set_color('none')
    # set the x-spine (see below for more info on `set_position`)
    if fy:
        ax.spines['left'].set_position('zero')
    # set the y-spine
    if fx:
        ax.spines['bottom'].set_position('zero')
    ax.xaxis.tick_bottom()
    ax.yaxis.tick_left()
    plt.xlabel(axes_names[0])
    plt.ylabel(axes_names[1])
    plt.show()


if __name__ == '__main__':
    build_graph()
